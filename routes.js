var express     = require('express');
var bodyParser  = require('body-parser');
var fs          = require('fs');
var path        = require('path');
var appRoot     = require('app-root-path');
var config      = require('./libs/config').get();

var auth        = require('./middleware/auth');
var apiRouter   = express.Router();
var handlers    = {};
var handlersDir = path.join(appRoot.path, "handlers");

// Bootstrap handlers
fs.readdirSync(handlersDir).forEach(function (file) {
	if (~file.indexOf('.js')) {
		handlers[file.replace('.js', '')] = require(path.join(handlersDir, file));
	}
});

// Api routes
apiRouter.post('/login', handlers.users.login);
apiRouter.post('/register', handlers.users.register);
apiRouter.get('/user', handlers.users.search);

apiRouter.get('/me', auth, handlers.users.getMe);
apiRouter.put('/me', auth, handlers.users.update);
apiRouter.get('/user/:id', auth, handlers.users.get);

apiRouter.get('/item', handlers.items.search);
apiRouter.get('/item/:id', handlers.items.get);
apiRouter.post('/item', auth, handlers.items.create);
apiRouter.put('/item/:id', auth, handlers.items.update);
apiRouter.delete('/item/:id', auth, handlers.items.remove);

apiRouter.post('/item/:id/image', auth, handlers.items.uploadImage);
apiRouter.delete('/item/:id/image', auth, handlers.items.removeImage);

module.exports.setup = function (app) {

	app.use('/images', express.static(config.get('fileUploads:uploadDir')));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	app.use('/api', apiRouter);
};