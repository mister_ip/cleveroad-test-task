var express = require('express');

var routes = require('./routes');
var logger = require('./middleware/expressLogger');
var crossDomain = require('./middleware/crossDomain');

var app = express();

app.use(crossDomain);
app.use(logger);

routes.setup(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function(err, req, res, next) {
  res.status(err.status || 500);

  if(err.body) {
    res.json(err.body);
  }
  else {
    res.end();
  }

});


module.exports = app;
