# REST API для сайта “Доска объявлений”

## Installation

```bash
$ npm install
```

## Run server

```bash
$ npm start
```

## Tests

  To run the test suite, first install the dependencies, then run `npm test`:

```bash
$ npm install
$ npm test
```

## Config files

```
configs/config.json -  config for application
configs/test-config.json - config for tests
```