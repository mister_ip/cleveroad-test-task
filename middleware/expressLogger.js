var logger  = require('../libs/logger');

module.exports = function(req, res, next) {
	logger.info(['HTTP REQUEST',
		req.method,
		req.url
	].join(' '));
	next();
};