var multer = require('multer');
var config = require('../libs/config').get();

var uploadDir = config.get('fileUploads:uploadDir');
var fileLimit = config.get('fileUploads:fileSize');

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, uploadDir);
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});

var validation = function(req, file, cb) {
	if (!file.mimetype || !(/(gif|jpe?g|png)$/i.test(file.mimetype))) {
		cb(new Error('Image only allows file types of GIF, PNG, JPG, JPEG'));
	}
	else {
		cb(null, true);
	}
};

var upload = multer({
	storage: storage,
	fileFilter: validation,
	limits: {
		fileSize: fileLimit
	}
});

module.exports = upload;