var _ = require('lodash');

var nextError = function(nextFunc, field, message) {
	nextFunc({
		status: 422,
		body:[{
			field: field,
			message: message
		}]
	})
};

var nextErrors = function(nextFunc, errorsArray) {
	nextFunc({
		status: 422,
		body: errorsArray
	})
};

var nextValidationErrors = function(nextFunc, errorsObject) {
	var errors = [];

	if(errorsObject.code === 11000) {
		var field = errorsObject.message.match(/\$(.*)_/i)[1];
		errors.push({
			field: field,
			message: 'Field `' + field + '` must be unique'
		});
	}
	else {
		_.forIn(errorsObject.errors, function(value, key) {
			errors.push({
				field: key,
				message: value.message
			});
		});
	}

	nextErrors(nextFunc, errors);
};

module.exports = {
	nextError: nextError,
	nextErrors: nextErrors,
	nextValidationErrors: nextValidationErrors
};