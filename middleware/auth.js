var jwt      = require('jsonwebtoken');
var config   = require('../libs/config').get();

module.exports = function (req, res, next) {
	var token  = req.headers.authorization;
	if(token) {
		jwt.verify(token, config.get('token:secret'), { algorithms: config.get('token:algorithm') },
		function (err, decoded) {
			if(err) {
				res.status(401).end();
			}
			else {
				req.user = decoded;
				next();
			}
		});
	}
	else {
		res.status(401).end();
	}
};