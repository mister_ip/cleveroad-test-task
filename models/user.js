var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require('bcrypt');
var jwt      = require('jsonwebtoken');
var config   = require('../libs/config').get();

var UserSchema = new Schema({
	phone     : { type: String },
	name      : {
		type: String,
		required: 'Name address is required'
	},
	email     : {
		type: String,
		trim: true,
		unique: true,
		required: 'Email address is required',
		validate: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Email address is not valid']
	},
	password  : {
		type: String,
		required: 'Password address is required'
	}
});

// Duplicate the ID field.
UserSchema.virtual('id').get(function(){
	return this._id.toHexString();
});

// Ensure virtual fields are serialised and delete _id property.
UserSchema.set('toJSON', {
	virtuals: true,
	transform: function (doc, ret) {
		delete ret._id;
		delete ret.__v;
		delete ret.password;
	}
});

UserSchema.pre('save', function (next) {
	var self = this;

	if (!self.isNew) {
		next();
	}
	else {
		bcrypt.genSalt(10, function (err, salt) {
			if(err) {
				next(err);
			}
			else {
				bcrypt.hash(self.password, salt, function (err, hash) {
					if(err) {
						next(err);
					}
					else {
						self.password = hash;
						next();
					}
				});
			}
		});
	}
});

UserSchema.statics.generateToken = function (user) {
	return jwt.sign({_id: user._id}, config.get('token:secret'), {
		algorithm: config.get('token:algorithm')
	});
};

UserSchema.methods.resetPassword = function(password, cb) {
	var self = this;

	bcrypt.genSalt(10, function (err, salt) {
		if (err) {
			cb(err);
		}
		else {
			bcrypt.hash(password, salt, function (err, hash) {
				if (err) {
					cb(err);
				}
				else {
					self.password = hash;
					self.save(function (err) {
						if (err) {
							cb(err);
						}
						else {
							cb(null);
						}
					});
				}
			});
		}
	});
};

UserSchema.methods.checkPassword = function(password, cb) {
	var self = this;

	bcrypt.compare(password, self.password, function (err, coincidence) {
		if (err) {
			cb(err);
		}
		else {
			if (coincidence) {
				cb(null, UserSchema.statics.generateToken(self));
			}
			else {
				cb(null, false);
			}
		}
	});
};

mongoose.model('User', UserSchema);

module.exports = UserSchema;