var winston        = require('winston');
var path           = require('path');
var appRoot        = require('app-root-path');
var config         = require('./config').get();

var winstonConfig =  {
	transports: [
		new(winston.transports.Console)({
			colorize: true,
			level: config.get('log:console:level')
		}),
		new (winston.transports.DailyRotateFile)({
			filename: path.join(appRoot.path, "logs", "_"),
			datePattern: 'yyyy-MM-dd.log',
			level: config.get('log:file:level')
		})
	]};

var logger = new (winston.Logger)(winstonConfig);

module.exports = logger;