var modelName = 'User';
var h         = require('../libs/helpers');
var User      = require('mongoose').model(modelName);
var async     = require('async');
var _         = require('lodash');

var login = function(req, res, next) {
	var email = req.body.email && req.body.email.trim();
	var password = req.body.password && req.body.password.trim();

	if(email && password) {
		User.findOne({email: email}, function (err, user) {
			if (err) {
				next(err);
			}
			else {
				if(user) {
					user.checkPassword(password, function(err, token) {
						if(token) {
							res.json({ token: token });
						}
						else {
							h.nextError(next, 'password', 'Wrong email or password');
						}
					});
				}
				else {
					h.nextError(next, 'email', 'Email does not exist')
				}
			}
		});
	}
	else {
		var errors = [];

		if(!email) {
			errors.push({
				field: 'email',
				message: 'Email can not be empty'
			});
		}

		if(!password) {
			errors.push({
				field: 'password',
				message: 'Password can not be empty'
			});
		}

		h.nextErrors(next, errors);
	}
};

var register = function(req, res, next) {
	User(req.body).save(function(err, user) {
		if(err) {
			h.nextValidationErrors(next, err);
		}
		else {
			res.json({token: User.generateToken(user)});
		}
	});
};

var getById = function(id, req, res, next) {
	if(req.user) {
		User.findById(id, function (err, user) {
			if(err){
				next(err);
			}
			else {
				if(user) {
					res.json(user);
				}
				else {
					next({status: 401});
				}
			}
		});
	}
	else {
		next({status: 401});
	}
};

var get = function(req, res, next) {
	getById(req.params.id, req, res, next);
};

var getMe = function(req, res, next) {
	getById(req.user._id, req, res, next);
};

var update = function (req, res, next) {
	var fields = req.body;

	User.findById(req.user._id, function(err, user) {
		if(err) {
			next(err);
		}
		else {
			async.series([
				function(cb){
					if(fields.current_password) {
						user.checkPassword(fields.current_password, function(err, token) {
							if(token) {
								if(fields.new_password) {
									user.resetPassword(fields.new_password, cb);
								}
								else {
									cb({errors: {new_password: {message: 'New password is required'}}});
								}
							}
							else {
								cb({errors: {current_password: {message: 'Wrong current password'}}});
							}
						});

					}
					else {
						cb(null);
					}
				},
				function(cb){
					_.forIn(fields, function(value, key) {
						user[key] = value;
					});
					user.save(function(err) {
						if(err){
							cb(err);
						}
						else {
							cb(null);
						}
					});
				}
			],
			function(err){
				if(err) {
					h.nextValidationErrors(next, err);
				}
				else {
					res.json(user);
				}
			});
		}
	});
};

var search = function(req, res, next) {
	var name = req.query.name;
	var email = req.query.email;
	var conditions = {};

	if(name || email) {
		var condition = [];

		if(name) {
			condition.push({name: name});
		}

		if(email) {
			condition.push({email: email});
		}

		conditions.$and = condition;
	}

	User.find(conditions, function(err, results) {
		if(err) {
			next(err);
		}
		else {
			res.json(results);
		}
	});
};

module.exports = {
	login: login,
	register: register,
	getMe: getMe,
	get: get,
	update: update,
	search: search
};