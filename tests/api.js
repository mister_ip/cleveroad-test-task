var should  = require('should');
var request = require('supertest');
var async   = require('async');
var _       = require('lodash');
var url     = require('url');

var configName = 'test-config';

describe('Routing', function() {
	var host, config, userModel, itemModel;

	var testUsers= [{
		phone: '+38012312234',
		name: 'Alex One',
		email: 'alex@mail.com',
		password: '123456'
	},{
		phone: '+38012312234',
		name: 'Alex Two',
		email: 'alex2@mail.com',
		password: '123456'
	},{
		phone: '+38012312234',
		name: 'Alex Two',
		email: 'alex22@mail.com',
		password: '123456'
	}];

	var testUser = testUsers[0];

	before(function(done) {
		async.series([
			function(cb){
				// Read config file
				config  = require('../libs/config');
				config.init(configName, cb);
			},
			function(cb){
				// Initialization database
				require('../libs/db').init(cb);
			},
			function(cb){
				// Initialization http server
				var app     = require('../app');
				var server  = require('../libs/server');
				server.init(app, cb);
			}
		], function() {
			userModel = require('mongoose').model('User');
			itemModel = require('mongoose').model('Item');
			config = config.get();
			host = config.get('host') + ':' + config.get('port');
			done();
		});
	});

	describe('Users routes', function(){
		describe('POST /api/login', function() {
			var path = '/api/login';
			var method = 'post';

			before(function(done) {
				createUser(testUser, done);
			});

			it('should return error trying to send empty body', function(done) {
				request(host)[method](path)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong email', function(done) {
				request(host)[method](path)
					.send({
						email: 'wrong@email.com'
					})
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong password', function(done) {
				request(host)[method](path)
					.send({
						email: testUser.email,
						password: 'wrongpassword'
					})
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should return token trying to send correct email and password', function(done) {
				request(host)[method](path)
					.send({
						email: testUser.email,
						password: testUser.password
					})
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkTokenResponse(res.body);
						done();
					});
			});

			after(clearUsers);
		});

		describe('POST /api/register', function() {
			var path = '/api/register';
			var method = 'post';

			it('should return error trying to send empty body', function(done) {
				request(host)[method](path)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should correctly register user', function(done) {
				request(host)[method](path)
					.send(testUser)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkTokenResponse(res.body);
						done();
					});
			});

			it('should return error trying to save duplicate email', function(done) {
				request(host)[method](path)
					.send(testUser)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong email', function(done) {
				var invalidUser = _.clone(testUser);
				invalidUser.email = 'invalid@email';

				request(host)[method](path)
					.send(invalidUser)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			after(clearUsers);
		});

		describe('GET /api/me', function() {
			var dbUser;
			var path = '/api/me';
			var method = 'get';

			before(function(done) {
				createUser(testUser, function(err, user) {
					dbUser = user;
					done();
				});
			});

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path, method, done);
			});

			it('should return error trying to send wrong auth token', function(done) {
				wrongAuthTokenRequest(host, path, method, done);
			});

			it('should correctly after send valid token', function(done) {
				var token = userModel.generateToken(dbUser);

				request(host)[method](path)
					.set('Authorization', token)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkUserResponse(res.body);
						done();
					});
			});

			after(clearUsers);
		});

		describe('PUT /api/me', function() {
			var path = '/api/me';
			var method = 'put';
			var dbUser;
			var token;

			before(function(done) {
				createUser(testUser, function(err, user) {
					dbUser = user;
					token = userModel.generateToken(user);
					done();
				})
			});

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path, method, done);
			});

			it('should correctly update an existing account', function(done) {
				var updatedUser = {
					phone: '+323242342342',
					email: 'new@mail.com'
				};

				request(host)[method](path)
					.set('Authorization', token)
					.send(updatedUser)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkUserResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong email', function(done) {
				var updatedUser = {
					email: 'invalid@mail'
				};

				request(host)[method](path)
					.set('Authorization', token)
					.send(updatedUser)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong current password', function(done) {
				var updatedUser = {
					current_password: 'wrong_password',
					new_password: '654321'
				};

				request(host)[method](path)
					.set('Authorization', token)
					.send(updatedUser)
					.expect(422)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkErrorResponse(res.body);
						done();
					});
			});

			it('should correctly change new password', function(done) {
				var updatedUser = {
					current_password: testUser.password,
					new_password: '654321'
				};

				request(host)[method](path)
					.set('Authorization', token)
					.send(updatedUser)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).have.property('id');
						done();
					});
			});

			after(clearUsers);
		});

		describe('GET /api/user/:id', function() {
			var path = '/api/user/';
			var method = 'get';
			var dbUser;
			var token;

			before(function(done) {
				createUser(testUser, function(err, user) {
					dbUser = user;
					token = userModel.generateToken(user);

					done();
				})
			});

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path + dbUser.id, method, done);
			});

			it('should return error trying to send without id', function(done) {
				request(host)[method](path)
					.set('Authorization', token)
					.expect(404)
					.end(function(err, res) {
						should(res.body).be.empty;
						done();
					});
			});

			it('should return error trying to send wrong id', function(done) {
				var wrongId = '66d2d7a7cde1f86845d3f7d5';

				request(host)[method](path + wrongId)
					.set('Authorization', token)
					.expect(404)
					.end(function(err, res) {
						should(res.body).be.empty;
						done();
					});
			});

			it('should correctly after send valid id', function(done) {
				request(host)[method](path + dbUser.id)
					.set('Authorization', token)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						checkUserResponse(res.body);
						done();
					});
			});

			after(clearUsers);
		});

		describe('GET /api/user?name=&email=', function() {
			var path = '/api/user';
			var method = 'get';

			before(function(done) {
				async.each(testUsers, function(user, cb){
					createUser(user, function(err) {
						if(err) {
							cb(err);
						}
						else {
							cb(null);
						}
					})
				}, function(err){
					if(err) {
						done(err);
					}
					else {
						done();
					}
				});
			});

			it('should return all users', function(done) {
				request(host)[method](path)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(3);

						res.body.map(function(user) {
							checkUserResponse(user);
						});

						done();
					});
			});

			it('should return one user after send name and email', function(done) {
				var user = testUsers[1];

				request(host)[method](url.format({pathname: path, query: {
						name: user.name,
						email: user.email
					}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(1);
						checkUserResponse(res.body[0]);
						done();
					});
			});

			it('should return empty array after send wrong name and email', function(done) {
				request(host)[method](url.format({pathname: path, query: {
						name: 'Wrong',
						email: 'wrong@email'
					}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(0);
						done();
					});
			});

			it('should return two users after send name', function(done) {
				var user = testUsers[1];

				request(host)[method](url.format({pathname: path, query: {
						name: user.name
					}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(2);

						res.body.map(function(user) {
							checkUserResponse(user);
						});

						done();
					});
			});

			it('should return one users after send email', function(done) {
				var user = testUsers[2];

				request(host)[method](url.format({pathname: path, query: {
						email: user.email
					}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(1);
						checkUserResponse(res.body[0]);
						should(res.body[0]).have.property('name', user.name);
						done();
					});
			});

			after(clearUsers);
		});
	});

	describe('Items routes', function(){
		var dbUser, token, userId;
		var otherUserId = '55d23fa8f103a65b43beb02b';
		var testItems = [{
			title: 'Notebook',
			price: 5500.00
		},{
			title: 'Printer',
			price: 3000.5
		},{
			title: 'Modem',
			price: 600
		}];

		var testItem = testItems[0];

		before(function(done) {
			createUser(testUser, function(err, user) {
				if(err) {
					done(err);
				}
				else {
					dbUser = user;
					userId = user._id;
					token = userModel.generateToken(user);
					done();
				}
			});
		});

		describe('POST /api/item', function() {
			var path = '/api/item';
			var method = 'post';

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path, method, done);
			});

			it('should return error trying to send empty body', function(done) {
				emptyBodyRequest(host, path, method, token, done);
			});

			it('should correctly create item', function(done) {
				request(host)[method](path)
					.set('Authorization', token)
					.send(testItem)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.statusCode).equal(200);
						checkItemResponse(res.body);
						done();
					});
			});

			it('should return error trying to send wrong price', function(done) {
				var invalidItem = {
					title: 'Some',
					price: 'invalid'
				};

				invalidBodyRequest(host, path, method, token, invalidItem, done);
			});

			it('should return error trying to send wrong title', function(done) {
				var invalidItem = {
					title: '',
					price: 5000.00
				};

				invalidBodyRequest(host, path, method, token, invalidItem, done);
			});

			after(clearItems);
		});

		describe('GET /api/item/:id', function() {
			var path = '/api/item/';
			var method = 'get';
			var dbItem;

			before(function(done) {
				createItem(testItem, userId, function(err, item) {
					if(err) {
						done(err);
					}
					else {
						dbItem = item;
						done(null);
					}
				});
			});

			it('should correctly after send valid item id', function(done) {
				request(host)[method](path + dbItem._id)
					.set('Authorization', token)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.statusCode).equal(200);
						checkItemResponse(res.body);
						done();
					});
			});

			it('should return error trying to send invalid id', function(done) {
				var invalidId = 'as234123sdfasfa2';
				checkNotFoundResponce(host, path + invalidId, method, token, done);
			});

			it('should return error trying to send id deleted items', function(done) {
				var oldId = '55d23fa8f103a65b43beb02b';

				checkNotFoundResponce(host, path + oldId, method, token, done);
			});

			after(clearItems);
		});

		describe('PUT /api/item/:id', function() {
			var path = '/api/item/';
			var method = 'put';
			var updatedItem = {
				title: 'Notebook New',
				price: 7700.00
			};
			var dbItem;

			before(function(done) {
				createItem(testItem, userId, function(err, item) {
					if(err) {
						done(err);
					}
					else {
						dbItem = item;
						done(null);
					}
				});
			});

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path + dbItem._id, method, done);
			});

			it('should correctly update an existing item', function(done) {
				request(host)[method](path + dbItem._id)
					.set('Authorization', token)
					.send(updatedItem)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.statusCode).equal(200);
						checkItemResponse(res.body);
						should(res.body).have.property('title', updatedItem.title);
						should(res.body).have.property('price', updatedItem.price);
						done();
					});
			});

			it('should return error trying to send wrong title', function(done) {
				var updatedItem = {
					title: 'yo'
				};

				invalidBodyRequest(host, path + dbItem._id, method, token, updatedItem, done);
			});

			it('should return error trying to send wrong price', function(done) {
				var updatedItem = {
					price: 'yo'
				};

				invalidBodyRequest(host, path + dbItem._id, method, token, updatedItem, done);
			});

			it('should return error trying to send id deleted items', function(done) {
				var oldId = '55d23fa8f103a65b43beb02b';

				checkNotFoundResponce(host, path + oldId, method, token, done);
			});

			it('should return error trying to update not user`s item', function(done) {
				createItem(testItem, otherUserId, function(err, item) {
					if(err) {
						done(err);
					}
					else {
						forbiddenBodyRequest(host, path + item._id, method, token, updatedItem, done);
					}
				});
			});

			after(clearItems);
		});

		describe('DELETE /api/item/:id', function() {
			var path = '/api/item/';
			var method = 'delete';

			var dbMineItem, dbNotMineItem;

			before(function(done) {
				async.series([
					function(cb){
						createItem(testItem, userId, function(err, item) {
							if(err) {
								cb(err);
							}
							else {
								dbMineItem = item;
								cb(null);
							}
						});
					},
					function(cb){
						createItem(testItem, otherUserId, function(err, item) {
							if(err) {
								cb(err);
							}
							else {
								dbNotMineItem = item;
								cb(null);
							}
						});
					}
				],
				function(err) {
					if(err) {
						done(err);
					}
					else {
						done(null);
					}
				});

			});

			it('should return error trying to send without auth token', function(done) {
				withoutAuthTokenRequest(host, path + dbMineItem._id, method, done);
			});

			it('should return error trying to send id deleted item', function(done) {
				var oldId = '55d23fa8f103a65b43beb02b';

				checkNotFoundResponce(host, path + oldId, method, token, done);
			});

			it('should return error trying to send wrong item id', function(done) {
				var wrongItemId = 'wrong_id';

				checkNotFoundResponce(host, path + wrongItemId, method, token, done);
			});

			it('should return error trying to deleted not user`s item', function(done) {
				forbiddenBodyRequest(host, path + dbNotMineItem._id, method, token, null, done);
			});

			it('should correctly delete an existing item', function(done) {
				request(host)[method](path + dbMineItem._id)
					.set('Authorization', token)
					.end(function(err, res) {
						should(res.statusCode).equal(200);
						should(res.body).be.empty;
						done();
					});
			});

			after(clearItems);
		});

		describe('GET /api/item?title=&user_id=order_by=&order_type=', function() {
			var path = '/api/item';
			var method = 'get';

			before(function(done) {
				async.each(testItems, function(user, cb){
					createItem(user, userId, function(err) {
						if(err) {
							cb(err);
						}
						else {
							cb(null);
						}
					})
				}, function(err){
					if(err) {
						done(err);
					}
					else {
						done();
					}
				});
			});

			it('should return all items', function(done) {
				request(host)[method](path)
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(3);

						res.body.map(function(item) {
							checkItemResponse(item);
						});

						done();
					});
			});

			it('should return one item after send title and user_id', function(done) {
				var item = testItems[1];

				request(host)[method](url.format({pathname: path, query: {
					title: item.title,
					user_id: userId
				}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(1);
						checkItemResponse(res.body[0]);
						done();
					});
			});

			it('should return empty array after send wrong title', function(done) {
				request(host)[method](url.format({pathname: path, query: {
					title: 'Wrong'
				}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(0);
						done();
					});
			});

			it('should return empty array after send wrong user_id', function(done) {
				var item = testItems[1];

				request(host)[method](url.format({pathname: path, query: {
					title: item.title,
					user_id: otherUserId
				}}))
					.expect(200)
					.expect('Content-Type', /json/)
					.end(function(err, res) {
						should(res.body).be.an.instanceOf(Array).and.have.lengthOf(0);
						done();
					});
			});

			after(clearItems);
		});

		after(clearUsers);
	});

	//===============================================================
	function checkNotFoundResponce(host, path, method, token, done) {
		request(host)[method](path)
			.set('Authorization', token)
			.end(function(err, res) {
				should(res.statusCode).equal(404);
				should(res.body).be.empty;
				done();
			});
	}

	function invalidBodyRequest(host, path, method, token, data, done) {
		request(host)[method](path)
			.set('Authorization', token)
			.send(data)
			.expect('Content-Type', /json/)
			.end(function(err, res) {
				should(res.statusCode).equal(422);
				checkErrorResponse(res.body);
				done();
			});
	}

	function forbiddenBodyRequest(host, path, method, token, data, done) {
		request(host)[method](path)
			.set('Authorization', token)
			.send(data)
			.expect('Content-Type', /json/)
			.end(function(err, res) {
				should(res.statusCode).equal(403);
				should(res.body).be.empty;
				done();
			});
	}

	function checkItemResponse(item) {
		should(item).have.property('id');
		should(item).have.property('title').and.be.type('string');
		should(item).have.property('price').and.be.type('number');
		should(item).have.property('created_at').and.be.type('number');
		should(item).have.property('user_id');
		should(item).have.property('user').and.be.type('object');
		checkUserResponse(item.user);
	}

	function emptyBodyRequest(host, path, method, token, done) {
		request(host)[method](path)
			.set('Authorization', token)
			.expect('Content-Type', /json/)
			.end(function(err, res) {
				should(res.statusCode).equal(422);
				checkErrorResponse(res.body);
				done();
			});
	}

	function wrongAuthTokenRequest(host, path, method, done) {
		var wrongToken = '3f5uh29fh3kjhpx7tyuioiugfvdfr9j8wi6onjf8';

		request(host)[method](path)
			.set('Authorization', wrongToken)
			.end(function(err, res) {
				should(res.statusCode).equal(401);
				should(res.body).be.empty;
				done();
			});
	}

	function withoutAuthTokenRequest(host, path, method, done) {
		request(host)[method](path)
			.end(function(err, res) {
				should(res.statusCode).equal(401);
				should(res.body).be.empty;
				done();
			});
	}

	function createUser(user, cb) {
		userModel(user).save(function(err, item) {
			if (err) {
				cb(err);
			}
			cb && cb(null, item);
		});
	}

	function createItem(data, userId, cb) {
		itemModel.saveWithUserId(data, userId, function(err, item) {
			if (err) {
				cb(err);
			}
			cb && cb(null, item);
		})
	}

	function clearUsers(done) {
		userModel.remove({}, function(err) {
			if (err) {
				done(err);
			}
			else {
				done();
			}
		});
	}
	
	function clearItems(done) {
		itemModel.remove({}, function(err) {
			if (err) {
				done(err);
			}
			else {
				done();
			}
		});
	}

	function checkErrorResponse(body) {
		should(body).be.an.instanceOf(Array).and.not.have.lengthOf(0);

		for(var i = 0; i < body.length; i++) {
			should(body[i]).have.property('field').and.be.type('string');
			should(body[i]).have.property('message').and.be.type('string');
		}
	}

	function checkTokenResponse(body) {
		should(body).be.an.instanceOf(Object)
			.and.have.property('token')
			.and.be.type('string');
	}

	function checkUserResponse(user) {
		should(user).have.property('id');
		should(user).have.property('phone').and.be.type('string');
		should(user).have.property('name').and.be.type('string');
		should(user).have.property('email').and.be.type('string');
	}
});