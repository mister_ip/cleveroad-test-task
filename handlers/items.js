var modelName   = 'Item';
var h           = require('../libs/helpers');
var Item        = require('mongoose').model(modelName);
var _           = require('lodash');
var fs          = require('fs');
var path        = require('path');
var appRoot     = require('app-root-path');
var config      = require('../libs/config').get();
var upload = require('../middleware/uploadImage').single('file');


var create = function(req, res, next) {
	var fields = req.body;
	fields.user = req.user._id;

	Item.saveWithUserId(req.body, req.user._id, function(err, item) {
		if(err) {
			h.nextValidationErrors(next, err);
		}
		else {
			getById(item._id, function(err, result) {
				if(err) {
					next(err);
				}
				else {
					res.json(result);
				}
			});
		}
	});
};

var get = function(req, res, next) {
	var id = req.params.id;

	if(id) {
		getById(id, function(err, item) {
			if(err) {
				next({status: 404});
			}
			else {
				if(item) {
					res.json(item);
				}
				else {
					next({status: 404});
				}
			}
		});
	}
	else {
		next({status: 404});
	}
};

var update = function(req, res, next) {
	var fields = req.body;
	var id = req.params.id;
	var userId = req.user._id;

	if(id) {
		Item.findById(id, function(err, item) {
			if(err) {
				next(err);
			}
			else {
				if(item) {
					if(item.user.toHexString() === userId) {
						_.forIn(fields, function(value, key) {
							item[key] = value;
						});
						item.save(function(err){
							if(err) {
								h.nextValidationErrors(next, err);
							}
							else {
								getById(item._id, function(err, fullItem) {
									if(err) {
										next(err);
									}
									else {
										res.json(fullItem);
									}
								});
							}
						});
					}
					else {
						next({status: 403});
					}
				}
				else {
					next({status: 404});
				}
			}
		});
	}
	else {
		next({status: 404});
	}
};

var remove = function(req, res, next) {
	checkAccessForUser(req, res, function(err, item) {
		if(err) {
			next(err);
		}
		else {
			item.remove(function(err) {
				if(err) {
					next(err);
				}
				else {
					res.status(200).end();
				}
			});
		}
	});
};

var search = function(req, res, next) {
	var title = req.query.title;
	var userId = req.query.user_id;
	var orderBy = req.query.order_by;
	var orderType = req.query.order_type;

	var conditions = {};
	var sortOptions = {};

	// Validation order type value
	if(orderType !== 'asc') {
		orderType = 'desc';
	}

	// Validation order by value
	if(orderBy !== 'price') {
		orderBy = 'created_at';
	}

	// Set sort options
	sortOptions[orderBy] = orderType;

	// Set search options
	if(title || userId) {
		var condition = [];

		if(title) {
			condition.push({title: title});
		}

		if(userId) {
			condition.push({user: userId});
		}

		conditions.$and = condition;
	}

	Item.find(conditions)
		.populate('user')
		.sort(sortOptions)
		.exec(function(err, results) {
			if(err) {
				next(err);
			}
			else {
				res.json(results);
			}
		});
};

var uploadImage = function (req, res, next) {
	checkAccessForUser(req, res, function(err, item) {
		if(err) {
			next(err);
		}
		else {
			upload(req, res, function (err) {
				if (err) {
					h.nextError(next, 'image', err.message);
				}
				else {
					item.image = req.file.filename;
					item.save(function(err) {
						if(err) {
							next(err);
						}
						else {
							getById(item._id, function(err, result) {
								if(err) {
									next(err);
								}
								else {
									res.json(result);
								}
							});
						}
					});
				}
			});
		}
	});
};

var removeImage = function(req, res, next) {
	checkAccessForUser(req, res, function(err, item) {
		if(err) {
			next(err);
		}
		else {
			if(item.image) {
				var imagePath = path.resolve(appRoot.path, config.get('fileUploads:uploadDir'), item.image);

				item.image = null;
				item.save(function(err) {
					if(err) {
						next(err);
					}
					else {
						fs.unlinkSync(imagePath);
						res.status(200).end();
					}
				});
			}
			else {
				next({status: 404});
			}
		}
	});
};

var checkAccessForUser = function(req, res, cb) {
	var itemId = req.params.id;
	var userId = req.user._id;

	if(itemId) {
		Item.findById(itemId, function (err, item) {
			if (err) {
				cb({status: 404});
			}
			else {
				if (item) {
					if (item.user.toHexString() === userId) {
						cb(null, item);
					}
					else {
						cb({status: 403});
					}
				}
				else {
					cb({status: 404});
				}
			}
		});
	}
	else {
		cb({status: 404});
	}

};

var getById = function (id, cb) {
	Item.findById(id)
		.populate('user')
		.exec(function (err, fullItem) {
			if (err) {
				cb(err);
			}
			else {
				cb(null, fullItem);
			}
		});
};

module.exports = {
	create: create,
	get: get,
	update: update,
	remove: remove,
	search: search,
	uploadImage: uploadImage,
	removeImage: removeImage
};