var nconf    = require('nconf');
var fs       = require('fs');
var path     = require('path');
var appRoot  = require('app-root-path');

var config   = null;
var defaults = {
	host: "http:/localhost",
	port: 3000,
	db: {
		uri: 'mongodb://localhost',
		options: {//http://mongoosejs.com/docs/connections.html
			server: {
				poolSize: 5,
				socketOptions: { keepAlive: 1 },
				autoReconnect: true
			}
		}
	},
	token: {
		secret: 'secret_key',
		algorithm: 'HS256'
	},
	defaultUser: {
		username         : 'admin',
		password         : 'password',
		expiresInMinutes : 120
	},
	fileUploads: {
		uploadDir: './uploads',
		fileSize: 5242880
	},
	log: {
		console:{
			level: 'info'
		},
		file: {
			level: 'warn'
		}
	}
};

var init = function(configName, cb) {
	initSync(configName || 'config');
	cb && cb();
};

var get = function() {
	if(!config) {
		initSync('config');
	}

	return config;
};

var initSync = function(configName) {
	var config_file_path = path.join(appRoot.path, 'configs', configName + '.json');

	if (!fs.existsSync(config_file_path)) {
		console.log('Config file `' + config_file_path + '` does not exists');
		process.exit(1);
	}

	nconf.argv()
		.env()
		.file({ file: config_file_path });

	nconf.defaults(defaults);

	config = nconf;
};


module.exports = {
	init: init,
	get: get
};