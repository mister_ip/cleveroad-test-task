var http   = require('http');
var config = require('./config').get();
var logger = require('./logger');
var port;

var createHttpServer = function(app, cb) {
	port = normalizePort(config.get('port'));
	app.set('port', port);

	var server = http.createServer(app);

	server.listen(port);
	server.on('error', onError);

	server.on('listening', function() {
		var addr = server.address();
		var bind = typeof addr === 'string'
			? 'pipe ' + addr
			: 'port ' + addr.port;
		logger.info('HTTP server listening on ' + bind);

		cb && cb();
	});
};

var normalizePort = function(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
};

var onError = function(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	var bind = typeof port === 'string'
		? 'Pipe ' + port
		: 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			logger.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			logger.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
};

module.exports = {
	init: createHttpServer
};