var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;
var ObjectId  = Schema.ObjectId;
var User      = require('./user');
var config    = require('../libs/config').get();
var url       = require('url');
var parseHost = url.parse(config.get('host'));
var port      = config.get('port');



var ItemSchema = new Schema({
	created_at : Number,
	title      : { type: String, required: 'Title is required'},
	price      : { type: Number, required: 'Price is required'},
	image      : String,
	user       : { type: ObjectId, ref: 'User' }
});

// Duplicate the id field.
ItemSchema.virtual('id').get(function(){
	return this._id.toHexString();
});

// Create user_id field
ItemSchema.virtual('user_id').get(function(){
	return this.user.id;
});

// Ensure virtual fields are serialised and transform some properties
ItemSchema.set('toJSON', {
	virtuals: true,
	transform: function (doc, ret) {
		delete ret._id;
		delete ret.__v;

		if(ret.image) {
			ret.image = url.format({
				protocol: parseHost.protocol || 'http',
				hostname: parseHost.host,
				port: port,
				pathname: 'images/' + ret.image
			});
		}
	}
});

ItemSchema.path('title').validate(function(title) {
	return title && title.length > 3;
}, 'Title should contain at least 3 characters');

ItemSchema.pre('save', function (next) {
	if(this.isNew) {
		this.created_at = Math.floor(Date.now() / 1000);
	}
	next();
});

ItemSchema.statics.saveWithUserId = function (data, userId, cb) {
	var self = this;
	data.user = userId;

	self(data).save(function(err, item) {
		if(err) {
			cb && cb(err);
		}
		else {
			cb && cb(null, item);
		}
	});
};


mongoose.model('Item', ItemSchema);