module.exports = function (req, res, next) {
	res.header("Access-Control-Allow-Origin", req.headers.origin);
	res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Content-Length, Accept, Authorization");
	res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');

	if ( 'OPTIONS' === req.method.toUpperCase() ){
		res.sendStatus(200);
	}
	else {
		next();
	}
};