var mongoose = require('mongoose');
var config   = require('./config').get();
var logger   = require('./logger');
var fs       = require('fs');
var path     = require('path');
var appRoot  = require('app-root-path');


module.exports.init = function(cb) {
	var uri = config.get('db:uri');
	var options = config.get('db:options');

	// Connect to mongodb
	var connect = function () {
		mongoose.connect(uri, options);
	};
	connect();

	// Connection successfully
	mongoose.connection.on('connected', function() {
		cb && cb();
		logger.info('MONGODB connected to ' + config.get('db:uri'));
	});

	// Error logging
	mongoose.connection.on('error', function(err) {
		logger.error('MONGODB ' + err);
	});

	// Bootstrap models
	var modelsDir = path.join(appRoot.path, "models");
	fs.readdirSync(modelsDir).forEach(function (file) {
		if (~file.indexOf('.js')) require(path.join(modelsDir, file));
	});
};